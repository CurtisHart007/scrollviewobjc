//
//  ViewController.m
//  ScrollView
//
//  Created by Carissa on 9/1/17.
//  Copyright © 2017 Carissa. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
        
        UIScrollView* scrollview = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];//Initalizing scrollview memory and allocating, and setting frame
        scrollview.contentSize = CGSizeMake(self.view.frame.size.width * 3, self.view.frame.size.height*3);
        scrollview.backgroundColor = [UIColor whiteColor];
        [scrollview setPagingEnabled:YES];
        [self.view addSubview:scrollview];
    
    
        // 1st column, 1st row View
        UIView* view1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        view1.backgroundColor = [UIColor redColor];
        [scrollview addSubview:view1];
    
        // 2nd column, 1st row View
        UIView* view2 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*1, 0, self.view.frame.size.width, self.view.frame.size.height)];
        view2.backgroundColor = [UIColor yellowColor];
        [scrollview addSubview:view2];
    
        // 3rd column, 1st row View
        UIView* view3 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*2, 0, self.view.frame.size.width, self.view.frame.size.height)];
        view3.backgroundColor = [UIColor blueColor];
        [scrollview addSubview:view3];
    
    
    
    
        // 1st column, 2nd row View
        UIView* view4 = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height*1, self.view.frame.size.width, self.view.frame.size.height)];
        view4.backgroundColor = [UIColor orangeColor];
        [scrollview addSubview:view4];
    
        // 2nd column, 2nd row View
        UIView* view5 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*1, self.view.frame.size.height*1, self.view.frame.size.width, self.view.frame.size.height)];
        view5.backgroundColor = [UIColor whiteColor];
        [scrollview addSubview:view5];
    
        // 3rd column, 2nd row View
        UIView* view6 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*2, self.view.frame.size.height*1, self.view.frame.size.width, self.view.frame.size.height)];
        view6.backgroundColor = [UIColor purpleColor];
        [scrollview addSubview:view6];
    
    
    
    
    
        // 1st column, 3rd row View
        UIView* view7 = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height*2, self.view.frame.size.width, self.view.frame.size.height)];
        view7.backgroundColor = [UIColor greenColor];
        [scrollview addSubview:view7];
    
        // 2nd column, 3rd row View
        UIView* view8 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*1, self.view.frame.size.height*2, self.view.frame.size.width, self.view.frame.size.height)];
        view8.backgroundColor = [UIColor blackColor];
        [scrollview addSubview:view8];
    
        // 3rd column, 3rd row View
        UIView* view9 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*2, self.view.frame.size.height*2, self.view.frame.size.width, self.view.frame.size.height)];
        view9.backgroundColor = [UIColor orangeColor];
        [scrollview addSubview:view9];
    }


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
